# Import necessary libraries and modules
import pygame
import random
# enemy def fix
import sys

# Define game constants
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480
ENEMY_WIDTH = 32
ENEMY_HEIGHT = 32
FPS = 60

# Define player properties
PLAYER_ACCELERATION = 0.5
PLAYER_MAX_SPEED = 8
PLAYER_JUMP_SPEED = 12
GRAVITY = 0.8

# Define game object properties
COIN_SCORE = 100
ENEMY_DAMAGE = 1

# Initialize pygame and set up the screen
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()

# Load game resources
background_image = pygame.image.load("background.jpg")
player_image = pygame.image.load("PngItem_1717950.png")
coin_image = pygame.image.load("Unbenannt.jpg")
enemy_image = pygame.image.load("PngItem_1603719.png")

# Define game classes
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = player_image
        self.rect = self.image.get_rect()
        self.velocity = pygame.math.Vector2(0, 0)
        self.position = pygame.math.Vector2(0, 0)
        self.acceleration = pygame.math.Vector2(0, 0)

    def update(self):
        # Apply gravity
        self.acceleration.y += GRAVITY

        # Apply acceleration
        self.velocity += self.acceleration
        if self.velocity.x > PLAYER_MAX_SPEED:
            self.velocity.x = PLAYER_MAX_SPEED
        if self.velocity.x < -PLAYER_MAX_SPEED:
            self.velocity.x = -PLAYER_MAX_SPEED

        # Move player
        self.position += self.velocity + 0.5 * self.acceleration
        self.rect.x = self.position.x
        self.rect.y = self.position.y

class Coin(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("coin.png").convert_alpha()
        self.rect = self.image.get_rect()

        # Set initial position randomly within screen bounds
        self.rect.x = random.randint(0, SCREEN_WIDTH - COIN_WIDTH)
        self.rect.y = random.randint(0, SCREEN_HEIGHT - COIN_HEIGHT)

        # Set random velocity
        self.velocity = pygame.math.Vector2(0, 0)
        while True:
            self.velocity.x = random.randint(-4, 4)
            self.velocity.y = random.randint(-4, 4)
            # Make sure the velocity is not zero
            if self.velocity.length() != 0:
                break

    # Define a method to update the coin's position
    def update(self):
        self.rect.move_ip(self.velocity)

        # Reverse the direction if the coin hits a wall
        if self.rect.left < 0 or self.rect.right > SCREEN_WIDTH:
            self.velocity.x *= -1
        if self.rect.top < 0 or self.rect.bottom > SCREEN_HEIGHT:
            self.velocity.y *= -1

# Inside the Enemy class definition
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("enemy.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.velocity = pygame.math.Vector2(0, 0)

    # Set initial position randomly within screen bounds
    self.rect.x = random.randint(0, SCREEN_WIDTH - ENEMY_WIDTH)
    self.rect.y = random.randint(0, SCREEN_HEIGHT - ENEMY_HEIGHT)

    # Set random velocity
    while True:
        self.velocity.x = random.randint(-4, 4)
        self.velocity.y = random.randint(-4, 4)
        # Make sure the velocity is not zero
        if self.velocity.length() != 0:
            break

    # Define a method to update the enemy's position
    def update(self):
        self.rect.move_ip(self.velocity)

        # Reverse the direction if the enemy hits a wall
        if self.rect.left < 0 or self.rect.right > SCREEN_WIDTH:
            self.velocity.x *= -1
        if self.rect.top < 0 or self.rect.bottom > SCREEN_HEIGHT:
            self.velocity.y *= -1

# Set up game objects
player = Player()
coins = pygame.sprite.Group()
enemies = pygame.sprite.Group()

for i in range(10):
    coin = Coin()
    coins.add(coin)

for i in range(5):
    enemy = Enemy()
    enemy.rect.x = random.randint(0, SCREEN_WIDTH - enemy.rect.width)
    enemy.rect.y = random.randint(0, SCREEN_HEIGHT - enemy.rect.height)
    enemy.velocity.x = random.randint(-4, 4)
    enemy.velocity.y = random.randint(-4, 4)
    enemies.add(enemy)

# Initialize game variables
score = 0
health = 100

# Set up game loop
running = True
while running:
    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                player.acceleration.y = -PLAYER_JUMP_SPEED

    # Update game objects
    player.update()
    coins.update()
    enemies.update()

    # Draw game objects
    screen.blit(background_image, (0, 0))
    screen.blit(player.image, player.rect)
    coins.draw(screen)
    enemies.draw(screen)

    # Draw score and health
    font = pygame.font.SysFont(None, 24)
    score_text = font.render("Score: {}".format(score), True, (255, 255, 255))
    screen.blit(score_text, (10, 10))
    health_text = font.render("Health: {}".format(health), True, (255, 255, 255))
    screen.blit(health_text, (10, 30))

    # Check for game over
    if health <= 0:
        running = False

    # Flip the display
    pygame.display.flip()

    # Limit the frame rate
    clock.tick(FPS)

# Clean up resources and quit pygame
pygame.quit()