# WordPress plugins

![Alt text]()<img src="https://s.w.org/style/images/about/WordPress-logotype-wmark.png" width="200" height="200" align="left">

---

## Motivation

At the end of 2018 all of our WordPress blogs had to be migrated from PHP 5.x to PHP 7.x as PHP 5.x is nearing EOL at the end of 2018. 
Using PHP 5.x from 2019 on will be a serious security vulnerability.

Thus, we had to make sure our themes and plugins are up to date and working with PHP 7.x.

---

## Introduction

In this directory we want to share all of those plugins we use in our projects which we migrated successfully to PHP 7.x.

We didn't create these plugins on our own. 
But we migrated and optimised the code so it would run and throw no errors with the PHP 7.1 and 7.2, latter the version we currently use.
Plus, we are probably the only ones who still update and maintain these plugins. :-/

We can only guarantee these will work with PHP 7.1 and 7.2 but it could be these will also be functional with PHP 7.3, 7.4, and so on.

Use at your own risk. 

Sharing is caring! <3

---

## Plugin overview

* Filosofo Old-Style Upload - a separate WordPress menu entry to upload your images the "old school WordPress" way when there was no complicated "media gallery feature"
Official description: Filosofo Old-Style Upload restores for WordPress 2.0+ the Upload feature found in earlier versions.

* Link Indication - a plugin which marks your outgoing links and indicates whether it's a hyperlink, PDF, Wikipedia page, etc.
Official description: Adds CSS class attributes to external links and optionally specific attributes to any other link types such as wikipedia.org, flickr, imdb, file extensions like .pdf or .zip, etc. Thereby you can indicate your links, e.g. by images, for characterizing your types of links. Furthermore you can add target="blank" to all external links, rel="nofollow" to specific hyperlinks or display websnapr preview thumbnails. Navigate to Settings → Link Indication.

* Spreadshop - Official description: Insert your personal SpreadShirt's shop (spreadshop) wrapped into Wordpress without any popup or iframe.

* GetRecentComments - a small plugin for showing the most recent comments to your weblog in your sidebar

* ttftitles - a neat plugin we use since ages to individualise the appearance of the titles of all of our weblogs

---

## Installation 

Download the plugins/plugin folders and upload them to your /wp-content/plugins directory.
Activate them from your WordPress menu entry "Plugins".

---

## Contributing

* have a look at the open [issues](https://gitlab.com/aethyx/opensource/issues "issues") and [pull requests](https://gitlab.com/aethyx/opensource/issues "pull requests")
* feel free to report bugs (feedback is much appreciated)
* suggest new features and improvements to both code and [documentation](https://gitlab.com/aethyx/opensource/wiki "documentation")
* propose solutions to existing problems
* submit pull requests :-)

---

## License

Check header of the plugins.

Otherwise licensed under [GPL V2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) and/or [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

---

## Donations

Setting up and creating this repository was a hell of work and consumed a lot of time and nerves, so donations are warmly welcome.

Please use the following addresses to send us donations:

BTC: 1Df8eapdFuxPxteZLfXnwvyVuo574psxKM

ETH: 0x0D2Ab63dfe70a7fA12f9d66eCfEA9dDc8F5173A8

XEM: NBZPMU-XES6ST-ITEBR3-IHAPTR-APGI3Y-RAAMHV-VZFJ
 
