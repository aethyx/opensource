# Ethereum buttons

![Alt text](https://www.ethereum.org/images/logos/ETHEREUM-ICON_Black_small.png "Ethereum Logo")

---

## Motivation

As we wanted to support Ethereum natively on AETHYX MEDIAE we were in need of buttons for donations.

We scraped the web but couldn't find images in the dimension we needed: 110x20px.

Not even 80x15 buttons were available at the time of searching (May 2018).

So we created two of them ourselves [following the official Ethereum visual identity guideline from 2015](https://www.ethereum.org/images/logos/Ethereum_Visual_Identity_1.0.0.pdf).

---

## Introduction

We couldn't decide which naming is more important for us. So we created two of them: one says simply "Ethereum" and the other "Donate Ether".

We are releasing both of them into the wild.

Feel free to use them on the web wherever you wish!

---

## Features

* using offcial font "Print clearly"
* official Ethereum logo, it's just a resized version to fit the button
* official background colour #ECF0F1
* beveled button
* two versions available
* *.png file extension
* size 110x20px

---

## Installation 

Just download the buttons from this repository and use them on the web wherever you want! :-)

---

## Contributing

* have a look at the open [issues](https://github.com/aethyx/opensource/issues "issues") and [pull requests](https://github.com/aethyx/opensource/issues "pull requests")
* feel free to report bugs (feedback is much appreciated)
* suggest new features and improvements to both code and [documentation](https://github.com/aethyx/opensource/wiki "documentation")
* propose solutions to existing problems
* submit pull requests :-)

---

## License

Licensed under [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
 
